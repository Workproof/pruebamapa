[
  {
    "idzona": "2",
    "nombre": "Zona 1",
    "color_hex": "#00CDE5",
    "subzonas": [
      {
        "idsubzona": "33",
        "idestado": "26",
        "estado_codigo": "SON",
        "estado_nombre": "Sonora"
      }
    ]
  },
  {
    "idzona": "3",
    "nombre": "Zona 2",
    "color_hex": "#009AE2",
    "subzonas": [
      {
        "idsubzona": "35",
        "idestado": "8",
        "estado_codigo": "CHIH",
        "estado_nombre": "Chihuahua"
      }
    ]
  },
  {
    "idzona": "4",
    "nombre": "Zona 3",
    "color_hex": "#0068DF",
    "subzonas": [
      {
        "idsubzona": "36",
        "idestado": "19",
        "estado_codigo": "NUE",
        "estado_nombre": "Nuevo León"
      }
    ]
  },
  {
    "idzona": "5",
    "nombre": "Zona 4",
    "color_hex": "#0068DF",
    "subzonas": [
      {
        "idsubzona": "37",
        "idestado": "24",
        "estado_codigo": "SAN",
        "estado_nombre": "San Luis Potosí"
      }
    ]
  },
  {
    "idzona": "6",
    "nombre": "Zona 5",
    "color_hex": "#0008DA",
    "subzonas": [
      {
        "idsubzona": "38",
        "idestado": "25",
        "estado_codigo": "SIN",
        "estado_nombre": "Sinaloa"
      }
    ]
  },
  {
    "idzona": "7",
    "nombre": "Zona 6",
    "color_hex": "#2500D7",
    "subzonas": [
      {
        "idsubzona": "39",
        "idestado": "1",
        "estado_codigo": "AGU",
        "estado_nombre": "Aguascalientes"
      }
    ]
  },
  {
    "idzona": "8",
    "nombre": "Zona 7",
    "color_hex": "#5300D4",
    "subzonas": [
      {
        "idsubzona": "40",
        "idestado": "11",
        "estado_codigo": "GUA",
        "estado_nombre": "Guanajuato"
      }
    ]
  },
  {
    "idzona": "9",
    "nombre": "Zona 8",
    "color_hex": "#7E00D2",
    "subzonas": [
      {
        "idsubzona": "41",
        "idestado": "15",
        "estado_codigo": "EST",
        "estado_nombre": "Estado de México"
      }
    ]
  },
  {
    "idzona": "10",
    "nombre": "Zona 9",
    "color_hex": "#A900CF",
    "subzonas": [
      {
        "idsubzona": "42",
        "idestado": "23",
        "estado_codigo": "QUI",
        "estado_nombre": "Quintana Roo"
      }
    ]
  },
  {
    "idzona": "11",
    "nombre": "Zona 10",
    "color_hex": "#CC00C5",
    "subzonas": [
      {
        "idsubzona": "43",
        "idestado": "17",
        "estado_codigo": "MOR",
        "estado_nombre": "Morelos"
      }
    ]
  },
  {
    "idzona": "12",
    "nombre": "Zona 11",
    "color_hex": "#C90097",
    "subzonas": [
      {
        "idsubzona": "44",
        "idestado": "10",
        "estado_codigo": "DUR",
        "estado_nombre": "Durango"
      }
    ]
  },
  {
    "idzona": "13",
    "nombre": "Zona 12",
    "color_hex": "#C90097",
    "subzonas": [
      {
        "idsubzona": "45",
        "idestado": "30",
        "estado_codigo": "VER",
        "estado_nombre": "Veracruz"
      }
    ]
  },
  {
    "idzona": "14",
    "nombre": "Zona 13",
    "color_hex": "#C7006B",
    "subzonas": [
      {
        "idsubzona": "46",
        "idestado": "27",
        "estado_codigo": "TAB",
        "estado_nombre": "Tabasco"
      }
    ]
  },
  {
    "idzona": "15",
    "nombre": "Zona 14",
    "color_hex": "#C4003F",
    "subzonas": [
      {
        "idsubzona": "47",
        "idestado": "20",
        "estado_codigo": "OAX",
        "estado_nombre": "Oaxaca"
      }
    ]
  },
  {
    "idzona": "16",
    "nombre": "Zona 15",
    "color_hex": "#C10015",
    "subzonas": [
      {
        "idsubzona": "48",
        "idestado": "23",
        "estado_codigo": "QUI",
        "estado_nombre": "Quintana Roo"
      }
    ]
  }
]