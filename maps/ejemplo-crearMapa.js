// Arreglos para las zonas, subzonas y escuelas
var g_lista_zonas = new Array();
var g_lista_subzonas = new Array();
var g_lista_escuelas = new Array();

// Arreglos específicos para el mapa
var g_mapa_subzonas = new Array();

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Crea el mapa e inicializa la variable g_mapa_subzonas que contiene un arreglo con todas las subzonas disponibles
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function crearMapa()
{
	var usuario_zonas = {};
	var zonas_codigos = {};
	var zonas_colores = {};
	g_lista_zonas = usuario_zonas;
	for (var i = 0; i < usuario_zonas.length; i++)
	{
		var zona_id = usuario_zonas[i].idzona;
		var zona_nombre = usuario_zonas[i].nombre;
		var zona_color_hex = usuario_zonas[i].color_hex;
		var zonas_subzonas = usuario_zonas[i].subzonas;
		for (var j = 0; j < zonas_subzonas.length; j++)
		{
			var subzona_estado_codigo = zonas_subzonas[j].estado_codigo;
			var subzona_estado_nombre = zonas_subzonas[j].estado_nombre;
			if (!zonas_codigos.hasOwnProperty(subzona_estado_codigo))
			{
				var subzona_estado_escuelas = zonas_subzonas[j].escuelas;
				var subzona_estado_escuelas_total = zonas_subzonas[j].escuelas_total;
				zonas_codigos[subzona_estado_codigo] = zona_nombre;
				g_mapa_subzonas.push(
				{
					zona_id: zona_id,
					zona_nombre: zona_nombre,
					estado_codigo: subzona_estado_codigo,
					estado_nombre: subzona_estado_nombre,
					escuelas: subzona_estado_escuelas,
					esculeas_total: subzona_estado_escuelas_total
				});
			}
		}
		zonas_colores[zona_nombre] = zona_color_hex;
	}
	$('#vmap').vectorMap(
	{
		map: 'mx_en',
		enableZoom: true,
		showTooltip: true,
		backgroundColor: '#a5bfdd',
		borderColor: '#818181',
		borderOpacity: 0.25,
		borderWidth: 1,
		color: '#f4f3f0',
		hoverColor: '#c9dfaf',
		hoverOpacity: null,
		normalizeFunction: 'polynomial',
		scaleColors: ['#b6d6ff', '#005ace'],
		selectedColor: '#c9dfaf',
		regionsSelectable: true,
		regionsSelectableOne: true,
		regionStyle:
		{
			initial:
			{
				fill: '#eee',
				'fill-opacity': 1,
				stroke: 'black',
				'stroke-width': 0.5,
				'stroke-opacity': 1
			},
			hover:
			{
				fill: '#000000',
				'fill-opacity': 0.5,
				cursor: 'pointer'
			},
			selected:
			{
				fill: '#0000'
			},
			selectedHover:
			{}
		},
		regionLabelStyle:
		{
			initial:
			{
				'font-family': 'Verdana',
				'font-size': '12',
				'font-weight': 'bold',
				cursor: 'default',
				fill: 'black'
			},
			hover:
			{
				cursor: 'pointer'
			}
		},
		series:
		{
			regions: [
			{
				values: zonas_codigos,
				scale: zonas_colores,
				normalizeFunction: 'polynomial'
			}]
		},
		onRegionSelected: function(element, code, region, isSelected)
		{
			// Click en un estado del mapa}
			
		}
	});
}